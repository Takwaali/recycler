package com.example.recycelrview.pojo;

import com.google.gson.annotations.SerializedName;

public class PostModel {

    @SerializedName("temp_min")
     private int temp_min;
    @SerializedName("temp_max")
   private int temp_max;

    public int getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(int temp_min) {
        this.temp_min = temp_min;
    }

    public int getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(int temp_max) {
        this.temp_max = temp_max;
    }
}

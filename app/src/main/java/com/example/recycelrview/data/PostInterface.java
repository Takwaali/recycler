package com.example.recycelrview.data;
import com.example.recycelrview.pojo.PostModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PostInterface {
    @GET("/data/2.5/forecast?q=London,us&mode=xml&appid=439d4b804bc8187953eb36d2a8c26a02")
    public Call<List<PostModel>> getPosts();
}
